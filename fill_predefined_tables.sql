-- fill predefined table "degrees"
INSERT INTO degrees VALUES('MSc', 'Master�s Degree');
INSERT INTO degrees VALUES('BSc', 'Bachelor�s Degree');

-- fill predefined table "work_status"
INSERT INTO work_status VALUES('H', 'Hired');
INSERT INTO work_status VALUES('WL', 'Workless');

-- fill predefined table "course_type"
INSERT INTO course_type VALUES('R', 'Regular Course');
INSERT INTO course_type VALUES('E', 'Elective Course');
INSERT INTO course_type VALUES('O', 'Optional Course');

-- fill predefined table "programs"
INSERT INTO programs(name, degrees_type_code) VALUES('Data Science', 'MSc');
INSERT INTO programs(name, degrees_type_code) VALUES('Digital Marketing and web design', 'MSc');
INSERT INTO programs(name, degrees_type_code) VALUES('Software Engineering and Management', 'MSc');
INSERT INTO programs(name, degrees_type_code) VALUES('Cybersecurity', 'MSc');
INSERT INTO programs(name, degrees_type_code) VALUES('Administrative Law and Administrative Procedure', 'MSc');
INSERT INTO programs(name, degrees_type_code) VALUES('Criminal Law', 'MSc');
INSERT INTO programs(name, degrees_type_code) VALUES('Constitutional Law', 'MSc');
INSERT INTO programs(name, degrees_type_code) VALUES('Civil and Family Law', 'MSc');
INSERT INTO programs(name, degrees_type_code) VALUES('International Law and Relations', 'MSc');
INSERT INTO programs(name, degrees_type_code) VALUES('Computing and Computer Science', 'BSc');
INSERT INTO programs(name, degrees_type_code) VALUES('Political Economy', 'BSc');
INSERT INTO programs(name, degrees_type_code) VALUES('Global Leadership and International Economic Relations', 'BSc');
INSERT INTO programs(name, degrees_type_code) VALUES('Accounting, control and analysis of business', 'BSc');
INSERT INTO programs(name, degrees_type_code) VALUES('Choreography', 'BSc');
INSERT INTO programs(name, degrees_type_code) VALUES('General Psychology', 'BSc');
INSERT INTO programs(name, degrees_type_code) VALUES('Management in the Field of Security and Public Order', 'BSc');

-- fill predefined table "continents"
INSERT INTO continents(id, name, continent_code) VALUES(1, 'Africa', 'AF');
INSERT INTO continents(id, name, continent_code) VALUES(2, 'Antarctica', 'AN');
INSERT INTO continents(id, name, continent_code) VALUES(3, 'Asia', 'AS');
INSERT INTO continents(id, name, continent_code) VALUES(4, 'Europe', 'EU');
INSERT INTO continents(id, name, continent_code) VALUES(5, 'North america', 'NA');
INSERT INTO continents(id, name, continent_code) VALUES(6, 'Oceania', 'OC');
INSERT INTO continents(id, name, continent_code) VALUES(7, 'South america', 'SA');

-- fill predefined table "business_fields"
INSERT INTO business_fields(business_field) VALUES ('Accounting / Finance');
INSERT INTO business_fields(business_field) VALUES ('Sales');
INSERT INTO business_fields(business_field) VALUES ('Social Entrepreneurship / Corporate Responsibility');
INSERT INTO business_fields(business_field) VALUES ('Retail');
INSERT INTO business_fields(business_field) VALUES ('Real Estate');
INSERT INTO business_fields(business_field) VALUES ('Marketing');
INSERT INTO business_fields(business_field) VALUES ('Software Development');
INSERT INTO business_fields(business_field) VALUES ('Human Resources');
INSERT INTO business_fields(business_field) VALUES ('Consulting');
INSERT INTO business_fields(business_field) VALUES ('Entrepreneurship / Small Business');
INSERT INTO business_fields(business_field) VALUES ('Event Planning & Hospitality');
INSERT INTO business_fields(business_field) VALUES ('Other');

-- fill predefined table "countries"
INSERT INTO countries(name, country_code, continent_id) VALUES('Algeria', 'DZ', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Angola', 'AO', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Benin', 'BJ', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Botswana', 'BW', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Burkina Faso', 'BF', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Burundi', 'BI', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Cameroon', 'CM', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Canary Islands', '', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Cape Verde', '', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Ceuta', '', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Chad', 'TD', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Comoros', 'KM', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Djibouti', 'DJ', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Egypt', 'EG', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Eritrea', 'ER', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Ethiopia', 'ET', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Equatorial Guinea', 'GQ', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('French Southern Territories', 'TF', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Ivory Coast', '', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Gabon', 'GA', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Gambia', 'GM', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Ghana', 'GH', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Guinea', 'GN', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Kenya', 'KE', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Lesotho', 'LS', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Liberia', 'LR', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Libya', 'LY', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Madagascar', 'MG', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Madeira', '', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Malawi', 'MW', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Mali', 'ML', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Mauritania', 'MR', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Mauritius', 'MU', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Mayotte', 'YT', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Melilla', '', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Morocco', 'MA', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Mozambique', 'MZ', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Namibia', 'NA', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Niger', 'NE', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Nigeria', 'NG', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Reunion', 'RE', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Rwanda', 'RW', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('São Tomé and Príncipe', 'ST', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Sahrawi Arab Democratic Republic', 'BL', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Senegal', 'SN', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Seychelles', 'SC', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Sierra Leone', 'SL', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Somalia', 'SO', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Somaliland', '', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('South Sudan', 'SS', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Sudan', 'SD', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Tanzania', 'TZ', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Togo', 'TG', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Tunisia', 'TN', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Uganda', 'UG', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Zambia', 'ZM', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Zimbabwe', 'ZW', 1);
INSERT INTO countries(name, country_code, continent_id) VALUES('Afghanistan', 'AF', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Armenia', 'AM', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Azerbaijan', 'AZ', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Bahrain', 'BH', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Bangladesh', 'BD', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Bhutan', 'BT', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Brunei', 'BN', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Cambodia', 'KH', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('China', 'CN', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Georgia', 'GE', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Hong Kong', 'HK', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('India', 'IN', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Indonesia', 'ID', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Iran', 'IR', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Iraq', 'IQ', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Israel', 'IL', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Japan', 'JP', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Jordan', 'JO', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Kazakhstan', 'KZ', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Kuwait', 'KW', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Kyrgyzstan', 'KG', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Laos', 'LA', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Lebanon', 'LB', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Macau', 'MO', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Malaysia', 'MY', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Maldives', 'MV', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Mongolia', 'MN', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Myanmar [Burma]', 'MM', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Nepal', 'NP', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('North Korea', 'KP', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Oman', 'OM', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Pakistan', 'PK', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Philippines', 'PH', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Qatar', 'QA', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Saudi Arabia', 'SA', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Singapore', 'SG', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('South Korea', 'KR', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Sri Lanka', 'LK', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Syria', 'SY', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Taiwan', 'TW', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Tajikistan', 'TJ', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Thailand', 'TH', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Turkmenistan', 'TM', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('United Arab Emirates', 'AE', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Uzbekistan', 'UZ', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Vietnam', 'VN', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Yemen', 'YE', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Albania', 'AL', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Andorra', 'AD', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Armenia ', 'AM', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Austria', 'AT', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Belarus', 'BY', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Belgium', 'BE', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Bosnia and Herzegovina', 'BA', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Bulgaria', 'BG', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Croatia', 'HR', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Cyprus', 'CY', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Czech Republic', 'CZ', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Denmark', 'DK', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Estonia', 'EE', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Faroe Islands', 'FO', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Finland', 'FI', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('France', 'FR', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Georgia', 'GE', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Germany', 'DE', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Gibraltar', 'GI', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Greece', 'GR', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Hungary', 'HU', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Ireland', 'IE', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Iceland', 'IS', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Isle of Man', 'IM', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Italy', 'IT', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Kosovo', 'XK', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Latvia', 'LV', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Liechtenstein', 'LI', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Lithuania', 'LT', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Sweden', 'LU', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Macedonia', 'MK', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Malta', 'MT', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Moldova', 'MD', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Monaco', 'MC', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Montenegro', 'ME', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Netherlands', 'NL', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Norway', 'NO', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Poland', 'PO', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Portugal', 'PT', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Romania', 'RO', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Russian Federation', 'RU', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('San Marino', 'SM', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Serbia', 'RS', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Slovakia', 'SK', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Slovenia', 'SI', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Spain', 'ES', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Sweden', 'SE', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Switzerland', 'CH', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Turkey', 'TR', 3);
INSERT INTO countries(name, country_code, continent_id) VALUES('Ukraine', 'UA', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('United Kingdom', 'GB', 4);
INSERT INTO countries(name, country_code, continent_id) VALUES('Vatican City State', 'VA', 4);

-- fill predefined table "courses"
INSERT INTO courses(id, name) VALUES(1, 'PROBABILITY AND STATISTICS');
INSERT INTO courses(id, name) VALUES(2, 'COMPUTER ARCHITECTURE AND OPERATING SYSTEMS');
INSERT INTO courses(id, name) VALUES(3, 'COMPUTER NETWORKS AND COMMUNICATIONS');
INSERT INTO courses(id, name) VALUES(4, 'DATABASES');
INSERT INTO courses(id, name) VALUES(5, 'PROGRAMMING');
INSERT INTO courses(id, name) VALUES(6, 'PUZZLE COURSE');
INSERT INTO courses(id, name) VALUES(7, 'PLANE COURSE');
INSERT INTO courses(id, name) VALUES(8, 'PLUG-IN COURSE');
INSERT INTO courses(id, name) VALUES(9, 'PROGRAMMING FOR DATA SCIENCE');
INSERT INTO courses(id, name) VALUES(10, 'SEMANTIC DATA');
INSERT INTO courses(id, name) VALUES(11, 'DATABASE SYSTEMS');
INSERT INTO courses(id, name) VALUES(12, 'RESEARCH AND DEVELOPMENT INTERNSHIP');
INSERT INTO courses(id, name) VALUES(13, 'DATA SCIENCE PRACTICES MASTERCLASS');
INSERT INTO courses(id, name) VALUES(14, 'INFORMATION MANAGEMENT MASTERCLASS');
INSERT INTO courses(id, name) VALUES(15, 'DISTRIBUTED AND CLOUD COMPUTING');
INSERT INTO courses(id, name) VALUES(16, 'MASTER THESIS');
INSERT INTO courses(id, name) VALUES(17, 'DATA MINING');
INSERT INTO courses(id, name) VALUES(18, 'CRYPTOGRAPHY');
INSERT INTO courses(id, name) VALUES(19, 'HIGH PERFORMANCE SCIENTIFIC COMPUTING');
INSERT INTO courses(id, name) VALUES(20, 'PROCESS MINING');
INSERT INTO courses(id, name) VALUES(21, 'DATA SECURITY');
INSERT INTO courses(id, name) VALUES(22, 'NEUROSCIENCE');
INSERT INTO courses(id, name) VALUES(23, 'NATURAL LANGUAGE PROCESSING');
INSERT INTO courses(id, name) VALUES(24, 'MATHEMATICAL FOUNDATIONS OF INFORMATICS');
INSERT INTO courses(id, name) VALUES(25, 'PROGRAMMING (С/С++)');
INSERT INTO courses(id, name) VALUES(26, 'COMPUTER ARCHITECTURE AND OPERATING SYSTEMS');
INSERT INTO courses(id, name) VALUES(27, 'DATA STRUCTURES AND ALGORITHMS');
INSERT INTO courses(id, name) VALUES(28, 'COMPUTER NETWORKS AND COMMUNICATIONS');
INSERT INTO courses(id, name) VALUES(29, 'OBJECT ORIENTED PROGRAMMING (JAVA)');
INSERT INTO courses(id, name) VALUES(30, 'VIRTUALIZATION AND CLOUD TECHNOLOGIES');

-- fill predefined table "program_courses"
INSERT INTO program_courses(id, program_id, course_id, course_type_code) VALUES(1, 1, 1, 'R');
INSERT INTO program_courses(id, program_id, course_id, course_type_code) VALUES(2, 1, 2, 'R');
INSERT INTO program_courses(id, program_id, course_id, course_type_code) VALUES(3, 1, 3, 'R');
INSERT INTO program_courses(id, program_id, course_id, course_type_code) VALUES(4, 1, 4, 'R');
INSERT INTO program_courses(id, program_id, course_id, course_type_code) VALUES(5, 1, 5, 'R');
INSERT INTO program_courses(id, program_id, course_id, course_type_code) VALUES(6, 1, 6, 'R');