package com.vfu.coopeducationsystem.repositories;

import com.vfu.coopeducationsystem.models.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CompaniesRepository extends JpaRepository<Company, Long> {

    boolean existsByCode(String code);

    boolean existsByName(String name);

    Company findByCode(String code);

    Company findByName(String name);

    Optional<Company> findById(Long id);

    List<Company> findAll();
}
