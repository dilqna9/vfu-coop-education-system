package com.vfu.coopeducationsystem.repositories;

import com.vfu.coopeducationsystem.models.Student;
import com.vfu.coopeducationsystem.models.WorkStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentsRepository extends JpaRepository<Student, Long> {

    boolean existsByStudentNumber(String studentNumber);

    List<Student> findAll();

    List<Student> findAllByWorkStatus(WorkStatus workStatus);

    Optional<Student> findById(Long id);

    Student findByStudentNumber(String studentNumber);
}
