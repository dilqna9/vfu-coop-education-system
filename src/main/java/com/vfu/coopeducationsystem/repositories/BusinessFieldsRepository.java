package com.vfu.coopeducationsystem.repositories;

import com.vfu.coopeducationsystem.models.BusinessField;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BusinessFieldsRepository extends JpaRepository<BusinessField, Long> {

    boolean existsByBusinessField(String businessField);

    BusinessField findByBusinessField(String businessField);

    List<BusinessField> findAll();
}
