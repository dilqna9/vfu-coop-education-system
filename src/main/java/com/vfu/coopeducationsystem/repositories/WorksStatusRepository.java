package com.vfu.coopeducationsystem.repositories;

import com.vfu.coopeducationsystem.models.WorkStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorksStatusRepository extends JpaRepository<WorkStatus, String> {

    boolean existsByWorkStatusName(String workStatusName);

    List<WorkStatus> findAll();

    WorkStatus findByWorkStatusName(String workStatusName);
}
