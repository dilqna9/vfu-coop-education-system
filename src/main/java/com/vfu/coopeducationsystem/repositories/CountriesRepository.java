package com.vfu.coopeducationsystem.repositories;

import com.vfu.coopeducationsystem.models.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountriesRepository extends JpaRepository<Country, Long> {

    boolean existsByName(String countryName);

    @Query(value = "SELECT * FROM countries c ORDER BY c.name", nativeQuery = true)
    List<Country> findAll();

    Country findByName(String countryName);
}
