package com.vfu.coopeducationsystem.repositories;

import com.vfu.coopeducationsystem.models.StudentProgramCourse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentProgramCoursesRepository extends JpaRepository<StudentProgramCourse, Long> {

    boolean existsByStudent_IdAndProgramCourses_Id(Long studentId, Long courseId);
}
