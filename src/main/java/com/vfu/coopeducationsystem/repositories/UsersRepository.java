package com.vfu.coopeducationsystem.repositories;

import com.vfu.coopeducationsystem.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {

    boolean existsByUsername(String username);

    List<User> findAllByEnabledIsTrue();

    List<User> findAllByEnabledIsFalse();

    User findByUsername(String username);
}
