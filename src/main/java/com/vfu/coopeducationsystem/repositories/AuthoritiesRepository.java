package com.vfu.coopeducationsystem.repositories;

import com.vfu.coopeducationsystem.models.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthoritiesRepository extends JpaRepository<Authority, String> {

    boolean existsByUsername(String username);

    Authority findByUsername(String username);
}
