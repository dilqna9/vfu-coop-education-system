package com.vfu.coopeducationsystem.repositories;

import com.vfu.coopeducationsystem.models.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersDetailsRepository extends JpaRepository<UserDetails, Long> {

    Optional<UserDetails> findById(Long id);
}
