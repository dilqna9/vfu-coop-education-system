package com.vfu.coopeducationsystem.repositories;

import com.vfu.coopeducationsystem.models.Program;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProgramsRepository extends JpaRepository<Program, Long> {

    boolean existsByName(String programName);

    List<Program> findAll();

    Program findByName(String programName);
}
