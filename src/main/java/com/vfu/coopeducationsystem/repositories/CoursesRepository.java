package com.vfu.coopeducationsystem.repositories;

import com.vfu.coopeducationsystem.models.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CoursesRepository extends JpaRepository<Course, Long> {

    boolean existsByName(String courseName);

    List<Course> findAll();

    Course findByName(String courseName);
}
