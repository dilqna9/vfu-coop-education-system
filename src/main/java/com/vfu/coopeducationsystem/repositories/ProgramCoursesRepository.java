package com.vfu.coopeducationsystem.repositories;

import com.vfu.coopeducationsystem.models.Program;
import com.vfu.coopeducationsystem.models.ProgramCourses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProgramCoursesRepository extends JpaRepository<ProgramCourses, Long> {

    List<ProgramCourses> findAllByProgram(Program program);

    ProgramCourses findByProgram_IdAndCourse_Id(Long programId, Long courseId);
}
