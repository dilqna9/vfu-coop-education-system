package com.vfu.coopeducationsystem.controllers;

import com.vfu.coopeducationsystem.exceptions.EntityNotFoundException;
import com.vfu.coopeducationsystem.models.Company;
import com.vfu.coopeducationsystem.services.contracts.CompaniesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/company")
public class CompaniesController {

    private final CompaniesService companiesService;

    @Autowired
    public CompaniesController(CompaniesService companiesService) {
        this.companiesService = companiesService;
    }

    @GetMapping()
    public String getCompanyInformation(@RequestParam Long companyId,
                                     Model model){
        try {
            Company company = companiesService.getCompanyById(companyId);
            model.addAttribute("company", company);
            return "company";
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}
