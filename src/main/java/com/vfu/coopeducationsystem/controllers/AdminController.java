package com.vfu.coopeducationsystem.controllers;

import com.vfu.coopeducationsystem.models.Company;
import com.vfu.coopeducationsystem.models.User;
import com.vfu.coopeducationsystem.services.contracts.CompaniesService;
import com.vfu.coopeducationsystem.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private final UsersService usersService;
    private final CompaniesService companiesService;

    @Autowired
    public AdminController(UsersService usersService, CompaniesService companiesService) {
        this.usersService = usersService;
        this.companiesService = companiesService;
    }

    @GetMapping
    public String showAdminPage(){
        return "admin";
    }

    @GetMapping("/users/enabled")
    public String browseEnabledUsers(Model model){
        List<User> users = usersService.getAllEnabledUsers();
        model.addAttribute("users", users);
        return "users-enabled";
    }

    @GetMapping("/users/disabled")
    public String browseDisabledUsers(Model model){
        List<User> users = usersService.getAllDisabledUsers();
        model.addAttribute("users", users);
        return "users-disabled";
    }

    @GetMapping("/companies")
    public String browseAllCompanies(Model model){
        List<Company> companies = companiesService.getAllCompanies();
        model.addAttribute("companies", companies);
        return "companies";
    }
}
