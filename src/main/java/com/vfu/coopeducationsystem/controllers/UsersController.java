package com.vfu.coopeducationsystem.controllers;

import com.vfu.coopeducationsystem.exceptions.EntityNotFoundException;
import com.vfu.coopeducationsystem.models.User;
import com.vfu.coopeducationsystem.models.UserDetails;
import com.vfu.coopeducationsystem.models.dtos.UserDetailsDto;
import com.vfu.coopeducationsystem.models.dtos.UserPasswordDto;
import com.vfu.coopeducationsystem.models.mappers.UserMapper;
import com.vfu.coopeducationsystem.services.contracts.AuthoritiesService;
import com.vfu.coopeducationsystem.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.*;

@Controller
@RequestMapping("/user")
public class UsersController {

    private final UsersService usersService;
    private final AuthoritiesService authoritiesService;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UsersController(UsersService usersService,
                           AuthoritiesService authoritiesService,
                           UserMapper userMapper,
                           PasswordEncoder passwordEncoder) {
        this.usersService = usersService;
        this.authoritiesService = authoritiesService;
        this.userMapper = userMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping()
    public String userInformation(@RequestParam String username,
                                  Model model,
                                  Principal principal){
        try {
            User user = usersService.getUserByUsername(username);
            model.addAttribute("user", user);

            if(principal == null){
                model.addAttribute("error", LOGIN_ERROR_MSG);
                return "error";
            }

            String role = authoritiesService.getAuthorityByUsername(principal.getName()).getAuthority();
            if(!role.equalsIgnoreCase("ROLE_ADMIN") && !username.equalsIgnoreCase(principal.getName())) {
                model.addAttribute("error", PROFILE_ACCESS_ERROR_MSG);
                return "error";
            }
            return "user";
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/password")
    public String changePassword(Model model){
        model.addAttribute("userPasswordDto", new UserPasswordDto());
        return "change-password";
    }

    @PostMapping("/password")
    public String changePassword(@Valid @ModelAttribute UserPasswordDto userPasswordDto,
                                 Principal principal,
                                 Model model){

        try {
            User user = usersService.getUserByUsername(principal.getName());

            if (!passwordEncoder.matches(userPasswordDto.getOldPassword(), user.getPassword())) {
                model.addAttribute("error", String.format(WRONG_PASSWORD_ERROR_MSG, "Old password"));
                return "error";
            }

            if (!userPasswordDto.getNewPassword().equals(userPasswordDto.getRetypeNewPassword())) {
                model.addAttribute("error", CONFIRM_PASSWORD_ERROR_MSG);
                return "error";
            }

            user.setPassword(passwordEncoder.encode(userPasswordDto.getNewPassword()));
            usersService.updateUser(user);
            return "redirect:/";
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/update")
    public String updateUserDetails(Model model, Principal principal){
        try {
            User user = usersService.getUserByUsername(principal.getName());
            UserDetailsDto userDetailsDto = userMapper.mapUserDetailsToUserDetailsDto(user.getUserDetails());
            model.addAttribute("userDetailsDto", userDetailsDto);
            return "update-user-details";
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/update")
    public String updateUserDetails(@Valid @ModelAttribute UserDetailsDto userDetailsDto,
                                    Principal principal,
                                    Model model){
        try {
            if(principal == null){
                model.addAttribute("error", LOGIN_ERROR_MSG);
                return "error";
            }
            User user = usersService.getUserByUsername(principal.getName());
            UserDetails userDetails = userMapper.mapUserDetailsDtoToUserDetails(userDetailsDto, user);
            user.setUserDetails(userDetails);
            usersService.updateUser(user);

            return "redirect:/user?username=" + user.getUsername();
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "update-user-details";
        }
    }
}
