package com.vfu.coopeducationsystem.controllers;

import com.vfu.coopeducationsystem.exceptions.EntityNotFoundException;
import com.vfu.coopeducationsystem.models.Company;
import com.vfu.coopeducationsystem.models.Student;
import com.vfu.coopeducationsystem.services.contracts.CompaniesService;
import com.vfu.coopeducationsystem.services.contracts.StudentsService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.logging.Logger;

@Controller
public class FilesController {

    private final CompaniesService companiesService;
    private final StudentsService studentsService;
    private final Logger logger = Logger.getLogger(FilesController.class.getName());

    @Autowired
    public FilesController(CompaniesService companiesService,
                           StudentsService studentsService) {
        this.companiesService = companiesService;
        this.studentsService = studentsService;
    }

    @GetMapping("company/{companyId}/image")
    public void renderCompanyLogo(@PathVariable Long companyId, HttpServletResponse response) {
        try {
            Company company = companiesService.getCompanyById(companyId);

            if(company.getLogo() != null){
                response.setContentType("image/jpeg");
                InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(company.getLogo()));
                IOUtils.copy(is, response.getOutputStream());
            }
        } catch (EntityNotFoundException | IOException e) {
            logger.warning(e.getMessage());
        }
    }

    @GetMapping("student/{studentId}/image")
    public void renderStudentProfilePicture(@PathVariable Long studentId, HttpServletResponse response){
        try {
            Student student = studentsService.getStudentById(studentId);

            if(student.getProfilePicture() != null){
                response.setContentType("image/jpeg");
                InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(student.getProfilePicture()));
                IOUtils.copy(is, response.getOutputStream());
            }
        }catch (EntityNotFoundException | IOException e){
            logger.warning(e.getMessage());
        }
    }

    @GetMapping("/download/{studentNumber}/{fileName:.+}")
    public ResponseEntity downloadCV(@PathVariable String studentNumber, @PathVariable String fileName){
        Student student = studentsService.getStudentByStudentNumber(studentNumber);
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .body(Base64.getDecoder().decode(student.getCV()));
    }
}
