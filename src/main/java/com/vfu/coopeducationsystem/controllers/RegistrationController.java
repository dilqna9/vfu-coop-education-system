package com.vfu.coopeducationsystem.controllers;

import com.vfu.coopeducationsystem.exceptions.EntityAlreadyExistsException;
import com.vfu.coopeducationsystem.models.*;
import com.vfu.coopeducationsystem.models.dtos.CompanyDto;
import com.vfu.coopeducationsystem.models.dtos.UserDto;
import com.vfu.coopeducationsystem.models.mappers.AddressMapper;
import com.vfu.coopeducationsystem.models.mappers.AuthorityMapper;
import com.vfu.coopeducationsystem.models.mappers.CompanyMapper;
import com.vfu.coopeducationsystem.models.mappers.UserMapper;
import com.vfu.coopeducationsystem.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.*;

@Controller
public class RegistrationController {

    private final UsersService usersService;
    private final CompaniesService companiesService;
    private final CountriesService countriesService;
    private final AddressesService addressesService;
    private final BusinessFieldsService businessFieldsService;
    private final UserMapper userMapper;
    private final UserDetailsManager userDetailsManager;
    private final AuthorityMapper authorityMapper;
    private final CompanyMapper companyMapper;
    private final AddressMapper addressMapper;

    @Autowired
    public RegistrationController(UsersService usersService,
                                  CompaniesService companiesService,
                                  CountriesService countriesService,
                                  AddressesService addressesService,
                                  BusinessFieldsService businessFieldsService,
                                  UserMapper userMapper,
                                  UserDetailsManager userDetailsManager,
                                  AuthorityMapper authorityMapper,
                                  CompanyMapper companyMapper,
                                  AddressMapper addressMapper) {
        this.usersService = usersService;
        this.companiesService = companiesService;
        this.countriesService = countriesService;
        this.addressesService = addressesService;
        this.businessFieldsService = businessFieldsService;
        this.userMapper = userMapper;
        this.userDetailsManager = userDetailsManager;
        this.authorityMapper = authorityMapper;
        this.companyMapper = companyMapper;
        this.addressMapper = addressMapper;
    }

    @ModelAttribute("companies")
    public List<Company> populateCompanies() {
        return companiesService.getAllCompanies();
    }

    @GetMapping("/register/user")
    public String showRegisterUserPage(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "register-user";
    }

    @PostMapping("/register/user")
    public String registerUser(@Valid @ModelAttribute UserDto userDto,
                               Model model) {

        if (userDetailsManager.userExists(userDto.getUsername())) {
            model.addAttribute("error", String.format(ALREADY_EXITS_ERROR_MSG, "User", "username", userDto.getUsername()));
            return "register-user";
        }

        if (!userDto.getPassword().equals(userDto.getRetypePassword())) {
            model.addAttribute("error", String.format(WRONG_PASSWORD_ERROR_MSG, "Password"));
            return "register-user";
        }

        UserDetails userDetails = userMapper.mapUserDtoToUserDetails(userDto);

        User user = userMapper.mapUserDtoToUser(userDto, userDetails);
        Authority authority = authorityMapper.mapUserDtoToAuthority(userDto);
        try {
            usersService.createUser(user, userDetails, authority);
        } catch (EntityAlreadyExistsException e) {
            model.addAttribute("error", e.getMessage());
            return "register-user";
        }

        return "register-confirmation";
    }

    @ModelAttribute("countries")
    public List<Country> populateCountries() {
        return countriesService.getAllCountries();
    }

    @ModelAttribute("businessFields")
    public List<BusinessField> populateBusinessFields() {
        return businessFieldsService.getAllBusinessFields();
    }

    @GetMapping("/register/company")
    public String showRegisterCompanyPage(Model model) {
        model.addAttribute("companyDto", new CompanyDto());
        return "register-company";
    }

    @PostMapping("/register/company")
    @Transactional
    public String registerCompany(@Valid @ModelAttribute CompanyDto companyDto,
                                  Model model) {

        Address address = addressMapper.mapCompanyDtoToAddress(companyDto);
        addressesService.createAddress(address);

        Company company = companyMapper.mapCompanyDtoToCompany(companyDto, address);

        if (!companyDto.getCompanyLogo().isEmpty()) {
            try {
                company.setLogo(Base64.getEncoder().encodeToString(companyDto.getCompanyLogo().getBytes()));
            } catch (IOException e) {
                model.addAttribute("error", String.format(REQUIRED_ERROR_MSG, "Company logo"));
                return "register-company";
            }
        }

        try {
            companiesService.createCompany(company);
        } catch (EntityAlreadyExistsException e) {
            model.addAttribute("error", e.getMessage());
            return "register-company";
        }

        return "register-confirmation";
    }
}
