package com.vfu.coopeducationsystem.controllers;

import com.vfu.coopeducationsystem.exceptions.EntityAlreadyExistsException;
import com.vfu.coopeducationsystem.exceptions.EntityNotFoundException;
import com.vfu.coopeducationsystem.models.*;
import com.vfu.coopeducationsystem.models.dtos.StudentDto;
import com.vfu.coopeducationsystem.models.dtos.StudentProgramCourseDto;
import com.vfu.coopeducationsystem.models.mappers.AddressMapper;
import com.vfu.coopeducationsystem.models.mappers.StudentMapper;
import com.vfu.coopeducationsystem.models.mappers.StudentProgramCourseMapper;
import com.vfu.coopeducationsystem.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.*;

@Controller
@RequestMapping("/student")
public class StudentsController {

    private final StudentsService studentsService;
    private final UsersService usersService;
    private final AuthoritiesService authoritiesService;
    private final AddressesService addressesService;
    private final CountriesService countriesService;
    private final ProgramsService programsService;
    private final WorksStatusService worksStatusService;
    private final StudentProgramCoursesService studentProgramCoursesService;
    private final ProgramCoursesService programCoursesService;
    private final StudentMapper studentMapper;
    private final AddressMapper addressMapper;
    private final StudentProgramCourseMapper studentProgramCourseMapper;

    @Autowired
    public StudentsController(StudentsService studentsService,
                              UsersService usersService,
                              AuthoritiesService authoritiesService,
                              AddressesService addressesService,
                              CountriesService countriesService,
                              ProgramsService programsService,
                              WorksStatusService worksStatusService,
                              StudentProgramCoursesService studentProgramCoursesService,
                              ProgramCoursesService programCoursesService,
                              StudentMapper studentMapper,
                              AddressMapper addressMapper,
                              StudentProgramCourseMapper studentProgramCourseMapper) {
        this.studentsService = studentsService;
        this.usersService = usersService;
        this.authoritiesService = authoritiesService;
        this.addressesService = addressesService;
        this.countriesService = countriesService;
        this.programsService = programsService;
        this.worksStatusService = worksStatusService;
        this.studentProgramCoursesService = studentProgramCoursesService;
        this.programCoursesService = programCoursesService;
        this.studentMapper = studentMapper;
        this.addressMapper = addressMapper;
        this.studentProgramCourseMapper = studentProgramCourseMapper;
    }

    @ModelAttribute("countries")
    public List<Country> populateCountries(){
        return countriesService.getAllCountries();
    }

    @ModelAttribute("programs")
    public List<Program> populatePrograms(){
        return programsService.getAllPrograms();
    }

    @ModelAttribute("worksStatus")
    public List<WorkStatus> populateWorksStatus(){
        return worksStatusService.getAllWorksStatus();
    }

    @GetMapping("/new")
    public String createStudent(Model model){
        model.addAttribute("studentDto", new StudentDto());
        return "create-student";
    }

    @PostMapping("/new")
    public String createStudent(@Valid @ModelAttribute StudentDto studentDto,
                                Principal principal,
                                Model model,
                                BindingResult bindingResult) {

        if(principal == null){
            model.addAttribute("error", NOT_ALLOWED);
            return "create-student";
        }

        if(bindingResult.hasErrors()){
            return "create-student";
        }

        String role = authoritiesService.getAuthorityByUsername(principal.getName()).getAuthority();
        if(!role.equalsIgnoreCase("ROLE_ADMIN")) {
            model.addAttribute("error", NOT_ALLOWED);
            return "create-student";
        }

        if (studentDto.getProfilePicture().isEmpty() || studentDto.getCV().isEmpty()) {
            model.addAttribute("error", NO_FILE_ATTACHED_ERROR_MSG);
            return "create-student";
        }

        Address address = addressMapper.mapStudentDtoToAddress(studentDto);
        addressesService.createAddress(address);

        Student student = studentMapper.mapStudentDtoToStudent(studentDto, address, principal.getName());
        try {
            student.setProfilePicture(Base64.getEncoder().encodeToString(studentDto.getProfilePicture().getBytes()));
            student.setCV(Base64.getEncoder().encodeToString(studentDto.getCV().getBytes()));
        } catch (IOException e) {
            model.addAttribute("error", e.getMessage());
            return "create-student";
        }

        try {
            studentsService.createStudent(student);
        }catch (EntityAlreadyExistsException e){
            model.addAttribute("error", e.getMessage());
            return "create-student";
        }

        return "index";
    }

    @GetMapping("/workless")
    public String browseAllWorklessStudents(Model model){
        List<Student> worklessStudents = studentsService
                .getAllStudentsByWorkStatus(worksStatusService.getWorkStatusByWorkStatusName("Workless"));
        model.addAttribute("worklessStudents", worklessStudents);
        return "workless-students";
    }

    @GetMapping("/all")
    public String browseAllStudents(Model model){
        List<Student> students = studentsService.getAllStudents();
        model.addAttribute("students", students);
        return "students";
    }

    @GetMapping()
    public String studentInformation(@RequestParam Long id, Model model, Principal principal){

        if(principal == null){
            model.addAttribute("error", LOGIN_ERROR_MSG);
            return "error";
        }

        try {
            Student student = studentsService.getStudentById(id);
            model.addAttribute("student", student);
            return "student";
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/grade")
    public String addStudentProgramCourses(@RequestParam Long studentId,
                                           Model model){
        List<ProgramCourses> programCourses = programCoursesService.getAllCoursesByStudentProgram(studentId);
        List<Course> courses = new ArrayList<>();
        for (ProgramCourses programCourse : programCourses) {
            courses.add(programCourse.getCourse());
        }
        model.addAttribute("courses", courses);
        model.addAttribute("studentProgramCourseDto", new StudentProgramCourseDto());
        model.addAttribute("studentId", studentId);

        return "add-grade";
    }

    @PostMapping("/grade")
    public String addStudentProgramCourses(@RequestParam Long studentId,
                                           @Valid @ModelAttribute StudentProgramCourseDto studentProgramCourseDto,
                                           Principal principal,
                                           Model model,
                                           BindingResult bindingResult) {

        if(principal == null){
            model.addAttribute("error", NOT_ALLOWED);
            return "error";
        }

        if(bindingResult.hasErrors()){
            return "error";
        }

        String role = authoritiesService.getAuthorityByUsername(principal.getName()).getAuthority();
        if(!role.equalsIgnoreCase("ROLE_ADMIN")) {
            model.addAttribute("error", NOT_ALLOWED);
            return "error";
        }

        StudentProgramCourse studentProgramCourse = studentProgramCourseMapper.mapStudentProgramCourseDtoToStudentProgramCourse(studentProgramCourseDto, studentId);
        try {
            studentProgramCoursesService.createStudentProgramCourse(studentProgramCourse);
        }catch (EntityAlreadyExistsException e){
            model.addAttribute("error", e.getMessage());
            return "error";
        }

        return "redirect:/student/grade?studentId=" + studentId;
    }

}
