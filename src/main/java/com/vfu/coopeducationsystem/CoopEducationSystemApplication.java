package com.vfu.coopeducationsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoopEducationSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoopEducationSystemApplication.class, args);
    }

}
