package com.vfu.coopeducationsystem.models;

import javax.persistence.*;

@Entity
@Table(name = "business_fields")
public class BusinessField {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String businessField;

    public BusinessField() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBusinessField() {
        return businessField;
    }

    public void setBusinessField(String businessField) {
        this.businessField = businessField;
    }
}
