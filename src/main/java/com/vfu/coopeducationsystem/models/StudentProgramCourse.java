package com.vfu.coopeducationsystem.models;

import javax.persistence.*;

@Entity
@Table(name = "student_program_courses")
public class StudentProgramCourse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "program_course_id")
    private ProgramCourses programCourses;

    private Integer grade;

    public StudentProgramCourse() {
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public ProgramCourses getProgramCourses() {
        return programCourses;
    }

    public void setProgramCourses(ProgramCourses programCourses) {
        this.programCourses = programCourses;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }
}
