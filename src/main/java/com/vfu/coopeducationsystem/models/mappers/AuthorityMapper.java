package com.vfu.coopeducationsystem.models.mappers;

import com.vfu.coopeducationsystem.models.Authority;
import com.vfu.coopeducationsystem.models.dtos.UserDto;
import org.springframework.stereotype.Component;

@Component
public class AuthorityMapper {

    public Authority mapUserDtoToAuthority(UserDto userDto) {
        Authority authority = new Authority();
        authority.setUsername(userDto.getUsername());
        if(userDto.getCompanyName().equalsIgnoreCase("Varna Free University")) {
            authority.setAuthority("ROLE_ADMIN");
        }else {
            authority.setAuthority("ROLE_COMPANY");
        }
        return authority;
    }
}
