package com.vfu.coopeducationsystem.models.mappers;

import com.vfu.coopeducationsystem.models.Company;
import com.vfu.coopeducationsystem.models.User;
import com.vfu.coopeducationsystem.models.UserDetails;
import com.vfu.coopeducationsystem.models.dtos.UserDetailsDto;
import com.vfu.coopeducationsystem.models.dtos.UserDto;
import com.vfu.coopeducationsystem.services.contracts.CompaniesService;
import com.vfu.coopeducationsystem.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final PasswordEncoder passwordEncoder;
    private final UsersService usersService;
    private final CompaniesService companiesService;

    @Autowired
    public UserMapper(PasswordEncoder passwordEncoder,
                      UsersService usersService,
                      CompaniesService companiesService) {
        this.passwordEncoder = passwordEncoder;
        this.usersService = usersService;
        this.companiesService = companiesService;
    }

    public UserDetails mapUserDtoToUserDetails(UserDto userDto) {
        UserDetails userDetails = new UserDetails();
        userDetails.setEmail(userDto.getEmail());
        userDetails.setFirstName(userDto.getFirstName());
        userDetails.setLastName(userDto.getLastName());
        return userDetails;
    }

    public User mapUserDtoToUser(UserDto userDto, UserDetails userDetails) {
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setEnabled(true);
        user.setUserDetails(userDetails);
        user.setCompany(companiesService.getCompanyByName(userDto.getCompanyName()));
        return user;
    }

    public UserDetails mapUserDetailsDtoToUserDetails(UserDetailsDto userDetailsDto, User user){

        UserDetails userDetails = user.getUserDetails();
        userDetails.setEmail(userDetailsDto.getEmail());
        userDetails.setFirstName(userDetailsDto.getFirstName());
        userDetails.setLastName(userDetailsDto.getLastName());

        return userDetails;
    }

    public UserDetailsDto mapUserDetailsToUserDetailsDto(UserDetails userDetails){
        UserDetailsDto userDetailsDto = new UserDetailsDto();
        userDetailsDto.setEmail(userDetails.getEmail());
        userDetailsDto.setFirstName(userDetails.getFirstName());
        userDetailsDto.setLastName(userDetails.getLastName());

        return userDetailsDto;
    }
}
