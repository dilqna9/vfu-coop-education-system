package com.vfu.coopeducationsystem.models.mappers;

import com.vfu.coopeducationsystem.models.Address;
import com.vfu.coopeducationsystem.models.Student;
import com.vfu.coopeducationsystem.models.dtos.StudentDto;
import com.vfu.coopeducationsystem.services.contracts.AddressesService;
import com.vfu.coopeducationsystem.services.contracts.ProgramsService;
import com.vfu.coopeducationsystem.services.contracts.UsersService;
import com.vfu.coopeducationsystem.services.contracts.WorksStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Component
public class StudentMapper {

    private final AddressesService addressesService;
    private final ProgramsService programsService;
    private final WorksStatusService worksStatusService;
    private final UsersService usersService;

    @Autowired
    public StudentMapper(AddressesService addressesService,
                         ProgramsService programsService,
                         WorksStatusService worksStatusService,
                         UsersService usersService) {
        this.addressesService = addressesService;
        this.programsService = programsService;
        this.worksStatusService = worksStatusService;
        this.usersService = usersService;
    }

    public Student mapStudentDtoToStudent(StudentDto studentDto, Address address, String creator){
        Student student = new Student();
        student.setFirstName(studentDto.getFirstName());
        student.setMiddleName(studentDto.getMiddleName());
        student.setLastName(studentDto.getLastName());
        student.setStudentNumber(studentDto.getStudentNumber());
        student.setEmail(studentDto.getEmail());
        student.setPhoneNumber(studentDto.getPhoneNumber());
        student.setSemester(studentDto.getSemester());
        student.setAddress(address);
        student.setProgram(programsService.getProgramByName(studentDto.getProgram()));
        student.setWorkStatus(worksStatusService.getWorkStatusByWorkStatusName(studentDto.getWorkStatus()));
        student.setUser(usersService.getUserByUsername(creator));

        if(studentDto.getCV().isEmpty() || studentDto.getCV() != null) {
            String fileName = StringUtils.cleanPath(studentDto.getCV().getOriginalFilename());
            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/download/")
                    .path(student.getStudentNumber())
                    .path("/")
                    .path(fileName)
                    .toUriString();
            student.setDownloadLink(fileDownloadUri);
        }

        return student;
    }
}
