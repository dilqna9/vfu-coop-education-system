package com.vfu.coopeducationsystem.models.mappers;

import com.vfu.coopeducationsystem.models.Address;
import com.vfu.coopeducationsystem.models.dtos.CompanyDto;
import com.vfu.coopeducationsystem.models.dtos.StudentDto;
import com.vfu.coopeducationsystem.services.contracts.CountriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {

    private final CountriesService countriesService;

    @Autowired
    public AddressMapper(CountriesService countriesService) {
        this.countriesService = countriesService;
    }

    public Address mapCompanyDtoToAddress(CompanyDto companyDto){
        Address address = new Address();
        address.setAddress(companyDto.getAddress());
        address.setCountry(countriesService.getCountryByName(companyDto.getCountry()));
        return address;
    }

    public Address mapStudentDtoToAddress(StudentDto studentDto){
        Address address = new Address();
        address.setAddress(studentDto.getAddress());
        address.setCountry(countriesService.getCountryByName(studentDto.getCountry()));
        return address;
    }
}
