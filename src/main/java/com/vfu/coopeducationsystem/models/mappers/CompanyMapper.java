package com.vfu.coopeducationsystem.models.mappers;

import com.vfu.coopeducationsystem.models.Address;
import com.vfu.coopeducationsystem.models.Company;
import com.vfu.coopeducationsystem.models.dtos.CompanyDto;
import com.vfu.coopeducationsystem.services.contracts.BusinessFieldsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CompanyMapper {

    private final BusinessFieldsService businessFieldsService;

    @Autowired
    public CompanyMapper(BusinessFieldsService businessFieldsService) {
        this.businessFieldsService = businessFieldsService;
    }

    public Company mapCompanyDtoToCompany(CompanyDto companyDto, Address address){
        Company company = new Company();
        company.setCode(companyDto.getCompanyCode());
        company.setName(companyDto.getCompanyName());
        company.setEmail(companyDto.getCompanyEmail());
        company.setPhone(companyDto.getCompanyPhone());
        company.setFax(companyDto.getCompanyFax());
        company.setAddress(address);
        company.setBusinessField(businessFieldsService.getByBusinessField(companyDto.getBussinessField()));
        return company;
    }
}
