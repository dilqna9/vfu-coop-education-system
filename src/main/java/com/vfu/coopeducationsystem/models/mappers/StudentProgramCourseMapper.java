package com.vfu.coopeducationsystem.models.mappers;

import com.vfu.coopeducationsystem.models.ProgramCourses;
import com.vfu.coopeducationsystem.models.StudentProgramCourse;
import com.vfu.coopeducationsystem.models.dtos.StudentProgramCourseDto;
import com.vfu.coopeducationsystem.services.contracts.CoursesService;
import com.vfu.coopeducationsystem.services.contracts.ProgramCoursesService;
import com.vfu.coopeducationsystem.services.contracts.StudentProgramCoursesService;
import com.vfu.coopeducationsystem.services.contracts.StudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StudentProgramCourseMapper {

    private final ProgramCoursesService programCoursesService;
    private final StudentProgramCoursesService studentProgramCoursesService;
    private final StudentsService studentsService;
    private final CoursesService coursesService;

    @Autowired
    public StudentProgramCourseMapper(ProgramCoursesService programCoursesService, StudentProgramCoursesService studentProgramCoursesService, StudentsService studentsService, CoursesService coursesService) {
        this.programCoursesService = programCoursesService;
        this.studentProgramCoursesService = studentProgramCoursesService;
        this.studentsService = studentsService;
        this.coursesService = coursesService;
    }

    public StudentProgramCourse mapStudentProgramCourseDtoToStudentProgramCourse(StudentProgramCourseDto studentProgramCourseDto, Long studentId){
        StudentProgramCourse studentProgramCourse = new StudentProgramCourse();
        studentProgramCourse.setStudent(studentsService.getStudentById(studentId));

        ProgramCourses programCourses = programCoursesService.getByProgramIdAndCourseId(
                studentsService.getStudentById(studentId).getProgram().getId(),
                coursesService.getCourseByName(studentProgramCourseDto.getProgramCourseName()).getId());
        studentProgramCourse.setProgramCourses(programCourses);

        studentProgramCourse.setGrade(studentProgramCourseDto.getGrade());

        return studentProgramCourse;
    }
}
