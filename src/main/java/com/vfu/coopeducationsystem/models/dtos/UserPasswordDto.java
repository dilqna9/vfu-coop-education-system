package com.vfu.coopeducationsystem.models.dtos;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserPasswordDto {

    private String oldPassword;

    @Size(min = 6, max = 68, message = "Password must be between 6 and 68 characters")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$")
    private String newPassword;

    private String retypeNewPassword;

    public UserPasswordDto() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getRetypeNewPassword() {
        return retypeNewPassword;
    }

    public void setRetypeNewPassword(String retypeNewPassword) {
        this.retypeNewPassword = retypeNewPassword;
    }
}
