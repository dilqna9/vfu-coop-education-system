package com.vfu.coopeducationsystem.models.dtos;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class StudentProgramCourseDto {

    private String programCourseName;

    @Min(value=2, message="Grade must be equal or greater than 2")
    @Max(value=6, message="Grade must be equal or less than 6")
    private Integer grade;

    public String getProgramCourseName() {
        return programCourseName;
    }

    public void setProgramCourseName(String programCourseName) {
        this.programCourseName = programCourseName;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }
}
