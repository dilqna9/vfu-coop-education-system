package com.vfu.coopeducationsystem.models;

import javax.persistence.*;

@Entity
@Table(name = "degrees")
public class Degree {

    @Id
    private String degreeTypeCode;

    private String degreeTypeName;

    public Degree() {
    }

    public String getDegreeTypeCode() {
        return degreeTypeCode;
    }

    public void setDegreeTypeCode(String degreeTypeCode) {
        this.degreeTypeCode = degreeTypeCode;
    }

    public String getDegreeTypeName() {
        return degreeTypeName;
    }

    public void setDegreeTypeName(String degreeTypeName) {
        this.degreeTypeName = degreeTypeName;
    }
}
