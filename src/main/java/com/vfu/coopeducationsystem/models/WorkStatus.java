package com.vfu.coopeducationsystem.models;

import javax.persistence.*;

@Entity
@Table(name = "work_status")
public class WorkStatus {

    @Id
    private String workStatusCode;

    private String workStatusName;

    public WorkStatus() {
    }

    public String getWorkStatusCode() {
        return workStatusCode;
    }

    public void setWorkStatusCode(String workStatusCode) {
        this.workStatusCode = workStatusCode;
    }

    public String getWorkStatusName() {
        return workStatusName;
    }

    public void setWorkStatusName(String work_status_name) {
        this.workStatusName = work_status_name;
    }
}
