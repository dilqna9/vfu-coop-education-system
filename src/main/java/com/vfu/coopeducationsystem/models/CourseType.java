package com.vfu.coopeducationsystem.models;

import javax.persistence.*;

@Entity
@Table(name = "course_type")
public class CourseType {

    @Id
    private String courseTypeCode;

    private String courseTypeName;

    public CourseType() {
    }

    public String getCourseTypeCode() {
        return courseTypeCode;
    }

    public void setCourseTypeCode(String courseTypeCode) {
        this.courseTypeCode = courseTypeCode;
    }

    public String getCourseTypeName() {
        return courseTypeName;
    }

    public void setCourseTypeName(String courseTypeName) {
        this.courseTypeName = courseTypeName;
    }
}
