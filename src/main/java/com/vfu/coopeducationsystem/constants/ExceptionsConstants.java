package com.vfu.coopeducationsystem.constants;

public class ExceptionsConstants {

    public static final String ALREADY_EXITS_ERROR_MSG = "%s with %s %s already exists.";
    public static final String NOT_FOUND_ERROR_MSG = "%s with %s %s does not exist.";
    public static final String WRONG_PASSWORD_ERROR_MSG = "% does not match.";
    public static final String LOGIN_ERROR_MSG = "You must log in to continue.";
    public static final String CONFIRM_PASSWORD_ERROR_MSG = "Password confirmation does not match new password.";
    public static final String PROFILE_ACCESS_ERROR_MSG = "You connot see this profile.";
    public static final String REQUIRED_ERROR_MSG = "%s is required.";
    public static final String NO_FILE_ATTACHED_ERROR_MSG = "You need to attach a profile picture and CV file for student.";

    public static final String NOT_ALLOWED = "The page you are trying to access requires additional authentication.";

}
