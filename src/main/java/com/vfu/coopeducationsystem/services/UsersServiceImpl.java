package com.vfu.coopeducationsystem.services;

import com.vfu.coopeducationsystem.exceptions.EntityAlreadyExistsException;
import com.vfu.coopeducationsystem.exceptions.EntityNotFoundException;
import com.vfu.coopeducationsystem.models.Authority;
import com.vfu.coopeducationsystem.models.User;
import com.vfu.coopeducationsystem.models.UserDetails;
import com.vfu.coopeducationsystem.repositories.AuthoritiesRepository;
import com.vfu.coopeducationsystem.repositories.UsersDetailsRepository;
import com.vfu.coopeducationsystem.repositories.UsersRepository;
import com.vfu.coopeducationsystem.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.ALREADY_EXITS_ERROR_MSG;
import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.NOT_FOUND_ERROR_MSG;

@Service
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    private final UsersDetailsRepository usersDetailsRepository;
    private final AuthoritiesRepository authoritiesRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository,
                            UsersDetailsRepository usersDetailsRepository,
                            AuthoritiesRepository authoritiesRepository1) {
        this.usersRepository = usersRepository;
        this.usersDetailsRepository = usersDetailsRepository;
        this.authoritiesRepository = authoritiesRepository1;
    }

    @Override
    public void createUser(User user, UserDetails userDetails, Authority authority) {

        if (usersRepository.existsByUsername(user.getUsername())) {
            throw new EntityAlreadyExistsException(
                    String.format(ALREADY_EXITS_ERROR_MSG, "User", "username", user.getUsername()));
        }

        usersDetailsRepository.saveAndFlush(userDetails);
        usersRepository.saveAndFlush(user);
        authoritiesRepository.saveAndFlush(authority);
    }

    @Override
    public void updateUser(User user) {

        UserDetails userDetails = user.getUserDetails();

        usersRepository.saveAndFlush(user);
        usersDetailsRepository.saveAndFlush(userDetails);
    }

    @Override
    public List<User> getAllEnabledUsers() {
        return usersRepository.findAllByEnabledIsTrue();
    }

    @Override
    public List<User> getAllDisabledUsers() {
        return usersRepository.findAllByEnabledIsFalse();
    }

    @Override
    public User getUserByUsername(String username) {

        if (!usersRepository.existsByUsername(username)) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "User", "username", username));
        }

        return usersRepository.findByUsername(username);
    }

    @Override
    public UserDetails getUserDetailsById(Long id) {
        return usersDetailsRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format(NOT_FOUND_ERROR_MSG, "UserDetails", "id", id)));
    }
}
