package com.vfu.coopeducationsystem.services;

import com.vfu.coopeducationsystem.exceptions.EntityNotFoundException;
import com.vfu.coopeducationsystem.models.Country;
import com.vfu.coopeducationsystem.repositories.CountriesRepository;
import com.vfu.coopeducationsystem.services.contracts.CountriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.NOT_FOUND_ERROR_MSG;

@Service
public class CountriesServiceImpl implements CountriesService {

    private final CountriesRepository countriesRepository;

    @Autowired
    public CountriesServiceImpl(CountriesRepository countriesRepository) {
        this.countriesRepository = countriesRepository;
    }

    @Override
    public List<Country> getAllCountries() {
        return countriesRepository.findAll();
    }

    @Override
    public Country getCountryByName(String countryName) {

        if(!countriesRepository.existsByName(countryName)){
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG,  "Country", "name", countryName));
        }
        return countriesRepository.findByName(countryName);
    }
}
