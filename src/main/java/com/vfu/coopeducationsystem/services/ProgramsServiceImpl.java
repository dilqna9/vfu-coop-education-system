package com.vfu.coopeducationsystem.services;

import com.vfu.coopeducationsystem.exceptions.EntityNotFoundException;
import com.vfu.coopeducationsystem.models.Program;
import com.vfu.coopeducationsystem.repositories.ProgramsRepository;
import com.vfu.coopeducationsystem.services.contracts.ProgramsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.NOT_FOUND_ERROR_MSG;

@Service
public class ProgramsServiceImpl implements ProgramsService {

    private final ProgramsRepository programsRepository;

    @Autowired
    public ProgramsServiceImpl(ProgramsRepository programsRepository) {
        this.programsRepository = programsRepository;
    }

    @Override
    public List<Program> getAllPrograms() {
        return programsRepository.findAll();
    }

    @Override
    public Program getProgramByName(String programName) {

        if (!programsRepository.existsByName(programName)) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "Program", "name", programName));
        }
        return programsRepository.findByName(programName);
    }
}
