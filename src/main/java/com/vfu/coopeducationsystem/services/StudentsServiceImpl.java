package com.vfu.coopeducationsystem.services;

import com.vfu.coopeducationsystem.exceptions.EntityAlreadyExistsException;
import com.vfu.coopeducationsystem.exceptions.EntityNotFoundException;
import com.vfu.coopeducationsystem.models.Student;
import com.vfu.coopeducationsystem.models.WorkStatus;
import com.vfu.coopeducationsystem.repositories.StudentsRepository;
import com.vfu.coopeducationsystem.services.contracts.StudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.ALREADY_EXITS_ERROR_MSG;
import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.NOT_FOUND_ERROR_MSG;

@Service
public class StudentsServiceImpl implements StudentsService {

    private final StudentsRepository studentsRepository;

    @Autowired
    public StudentsServiceImpl(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    @Override
    public void createStudent(Student student) {

        if(studentsRepository.existsByStudentNumber(student.getStudentNumber())){
            throw new EntityAlreadyExistsException(
                    String.format(ALREADY_EXITS_ERROR_MSG, "Student", "student number", student.getStudentNumber()));
        }
        studentsRepository.saveAndFlush(student);
    }

    @Override
    public List<Student> getAllStudents() {
        return studentsRepository.findAll();
    }

    @Override
    public List<Student> getAllStudentsByWorkStatus(WorkStatus workStatus) {
        return studentsRepository.findAllByWorkStatus(workStatus);
    }

    @Override
    public Student getStudentById(Long id) {
        return studentsRepository.findById(id)
                .orElseThrow(() ->
                        new EntityNotFoundException(String.format(NOT_FOUND_ERROR_MSG, "Student", "id", id)));
    }

    @Override
    public Student getStudentByStudentNumber(String studentNumber) {

        if(!studentsRepository.existsByStudentNumber(studentNumber)){
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "Student", "student number", studentNumber));
        }
        return studentsRepository.findByStudentNumber(studentNumber);
    }
}
