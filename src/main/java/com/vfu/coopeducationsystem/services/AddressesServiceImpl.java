package com.vfu.coopeducationsystem.services;

import com.vfu.coopeducationsystem.models.Address;
import com.vfu.coopeducationsystem.repositories.AddressesRepository;
import com.vfu.coopeducationsystem.services.contracts.AddressesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressesServiceImpl implements AddressesService {

    private final AddressesRepository addressesRepository;

    @Autowired
    public AddressesServiceImpl(AddressesRepository addressesRepository) {
        this.addressesRepository = addressesRepository;
    }

    @Override
    public void createAddress(Address address) {
        addressesRepository.saveAndFlush(address);
    }
}
