package com.vfu.coopeducationsystem.services.contracts;

import com.vfu.coopeducationsystem.models.Authority;
import com.vfu.coopeducationsystem.models.User;
import com.vfu.coopeducationsystem.models.UserDetails;

import java.util.List;

public interface UsersService {

    void createUser(User user, UserDetails userDetails, Authority authority);

    void updateUser(User user);

    List<User> getAllEnabledUsers();

    List<User> getAllDisabledUsers();

    User getUserByUsername(String username);

    UserDetails getUserDetailsById(Long id);
}
