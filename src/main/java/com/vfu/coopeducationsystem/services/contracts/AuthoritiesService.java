package com.vfu.coopeducationsystem.services.contracts;

import com.vfu.coopeducationsystem.models.Authority;

public interface AuthoritiesService {

    Authority getAuthorityByUsername(String username);
}
