package com.vfu.coopeducationsystem.services.contracts;

import com.vfu.coopeducationsystem.models.Company;

import java.util.List;

public interface CompaniesService {

    void createCompany(Company company);

    Company getCompanyByCode(String code);

    Company getCompanyByName(String name);

    Company getCompanyById(Long id);

    List<Company> getAllCompanies();
}
