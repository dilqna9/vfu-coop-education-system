package com.vfu.coopeducationsystem.services.contracts;

import com.vfu.coopeducationsystem.models.BusinessField;

import java.util.List;

public interface BusinessFieldsService {

    BusinessField getByBusinessField(String businessField);

    List<BusinessField> getAllBusinessFields();
}
