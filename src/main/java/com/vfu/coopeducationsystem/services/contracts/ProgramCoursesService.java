package com.vfu.coopeducationsystem.services.contracts;

import com.vfu.coopeducationsystem.models.ProgramCourses;

import java.util.List;

public interface ProgramCoursesService {

    List<ProgramCourses> getAllCoursesByStudentProgram(Long studentId);

    ProgramCourses getByProgramIdAndCourseId(Long programId, Long courseId);
}
