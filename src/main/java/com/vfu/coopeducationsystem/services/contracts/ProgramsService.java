package com.vfu.coopeducationsystem.services.contracts;

import com.vfu.coopeducationsystem.models.Program;

import java.util.List;

public interface ProgramsService {

    List<Program> getAllPrograms();

    Program getProgramByName(String programName);
}
