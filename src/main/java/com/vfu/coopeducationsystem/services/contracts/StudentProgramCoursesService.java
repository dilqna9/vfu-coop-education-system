package com.vfu.coopeducationsystem.services.contracts;

import com.vfu.coopeducationsystem.models.StudentProgramCourse;

public interface StudentProgramCoursesService {

    void createStudentProgramCourse(StudentProgramCourse studentProgramCourse);
}
