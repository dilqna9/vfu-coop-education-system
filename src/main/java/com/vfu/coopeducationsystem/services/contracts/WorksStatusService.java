package com.vfu.coopeducationsystem.services.contracts;

import com.vfu.coopeducationsystem.models.WorkStatus;

import java.util.List;

public interface WorksStatusService {

    List<WorkStatus> getAllWorksStatus();

    WorkStatus getWorkStatusByWorkStatusName(String workStatusName);
}
