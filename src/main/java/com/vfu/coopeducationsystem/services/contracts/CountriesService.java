package com.vfu.coopeducationsystem.services.contracts;

import com.vfu.coopeducationsystem.models.Country;

import java.util.List;

public interface CountriesService {

    List<Country> getAllCountries();

    Country getCountryByName(String countryName);
}
