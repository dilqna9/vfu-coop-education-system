package com.vfu.coopeducationsystem.services.contracts;

import com.vfu.coopeducationsystem.models.Student;
import com.vfu.coopeducationsystem.models.WorkStatus;

import java.util.List;

public interface StudentsService {

    void createStudent(Student student);

    List<Student> getAllStudents();

    List<Student> getAllStudentsByWorkStatus(WorkStatus workStatus);

    Student getStudentById(Long id);

    Student getStudentByStudentNumber(String studentNumber);
}
