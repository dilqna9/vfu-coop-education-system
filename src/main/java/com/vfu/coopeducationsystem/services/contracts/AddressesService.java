package com.vfu.coopeducationsystem.services.contracts;

import com.vfu.coopeducationsystem.models.Address;

public interface AddressesService {

    void createAddress(Address address);
}
