package com.vfu.coopeducationsystem.services.contracts;

import com.vfu.coopeducationsystem.models.Course;

public interface CoursesService {

    Course getCourseByName(String courseName);
}
