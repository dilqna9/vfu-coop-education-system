package com.vfu.coopeducationsystem.services;

import com.vfu.coopeducationsystem.exceptions.EntityNotFoundException;
import com.vfu.coopeducationsystem.models.BusinessField;
import com.vfu.coopeducationsystem.repositories.BusinessFieldsRepository;
import com.vfu.coopeducationsystem.services.contracts.BusinessFieldsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.NOT_FOUND_ERROR_MSG;

@Service
public class BusinessFieldsServiceImpl implements BusinessFieldsService {

    private final BusinessFieldsRepository businessFieldsRepository;

    @Autowired
    public BusinessFieldsServiceImpl(BusinessFieldsRepository businessFieldsRepository) {
        this.businessFieldsRepository = businessFieldsRepository;
    }

    @Override
    public BusinessField getByBusinessField(String businessField) {

        if(!businessFieldsRepository.existsByBusinessField(businessField)){
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG,  "Business Field", "name", businessField));
        }
        return businessFieldsRepository.findByBusinessField(businessField);
    }

    @Override
    public List<BusinessField> getAllBusinessFields() {
        return businessFieldsRepository.findAll();
    }
}
