package com.vfu.coopeducationsystem.services;

import com.vfu.coopeducationsystem.exceptions.EntityNotFoundException;
import com.vfu.coopeducationsystem.models.Course;
import com.vfu.coopeducationsystem.repositories.CoursesRepository;
import com.vfu.coopeducationsystem.services.contracts.CoursesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.NOT_FOUND_ERROR_MSG;

@Service
public class CoursesServiceImpl implements CoursesService {

    private final CoursesRepository coursesRepository;

    @Autowired
    public CoursesServiceImpl(CoursesRepository coursesRepository) {
        this.coursesRepository = coursesRepository;
    }

    @Override
    public Course getCourseByName(String courseName) {
        if(!coursesRepository.existsByName(courseName)){
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "Course", "name", courseName));
        }
        return coursesRepository.findByName(courseName);
    }
}
