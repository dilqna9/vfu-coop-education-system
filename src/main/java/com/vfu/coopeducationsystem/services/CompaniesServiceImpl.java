package com.vfu.coopeducationsystem.services;

import com.vfu.coopeducationsystem.exceptions.EntityAlreadyExistsException;
import com.vfu.coopeducationsystem.exceptions.EntityNotFoundException;
import com.vfu.coopeducationsystem.models.Company;
import com.vfu.coopeducationsystem.repositories.CompaniesRepository;
import com.vfu.coopeducationsystem.services.contracts.CompaniesService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.ALREADY_EXITS_ERROR_MSG;
import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.NOT_FOUND_ERROR_MSG;

@Service
public class CompaniesServiceImpl implements CompaniesService {

    private final CompaniesRepository companiesRepository;

    @Autowired
    public CompaniesServiceImpl(CompaniesRepository companiesRepository) {
        this.companiesRepository = companiesRepository;
    }

    @Override
    public void createCompany(Company company) {
        if (companiesRepository.existsByCode(company.getCode())) {
            throw new EntityAlreadyExistsException(
                    String.format(ALREADY_EXITS_ERROR_MSG, "Company", "code", company.getCode()));
        }
        companiesRepository.saveAndFlush(company);
    }

    @Override
    public Company getCompanyByCode(String code) {

        if (!companiesRepository.existsByCode(code)) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "Company", "code", code));
        }
        return companiesRepository.findByCode(code);
    }

    @Override
    public Company getCompanyByName(String name) {

        if (!companiesRepository.existsByName(name)) {
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "Company", "name", name));
        }
        return companiesRepository.findByName(name);
    }

    @Override
    public Company getCompanyById(Long id) {
        return companiesRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format(NOT_FOUND_ERROR_MSG, "Company", "id", id)));
    }

    @Override
    public List<Company> getAllCompanies() {
        return companiesRepository.findAll();
    }
}
