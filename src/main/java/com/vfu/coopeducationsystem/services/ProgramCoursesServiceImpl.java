package com.vfu.coopeducationsystem.services;

import com.vfu.coopeducationsystem.models.ProgramCourses;
import com.vfu.coopeducationsystem.models.Student;
import com.vfu.coopeducationsystem.repositories.ProgramCoursesRepository;
import com.vfu.coopeducationsystem.repositories.ProgramsRepository;
import com.vfu.coopeducationsystem.services.contracts.ProgramCoursesService;
import com.vfu.coopeducationsystem.services.contracts.StudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProgramCoursesServiceImpl implements ProgramCoursesService {

    private final ProgramCoursesRepository programCoursesRepository;
    private final ProgramsRepository programsRepository;
    private final StudentsService studentsService;

    @Autowired
    public ProgramCoursesServiceImpl(ProgramCoursesRepository programCoursesRepository,
                                     ProgramsRepository programsRepository,
                                     StudentsService studentsService) {
        this.programCoursesRepository = programCoursesRepository;
        this.programsRepository = programsRepository;
        this.studentsService = studentsService;
    }

    @Override
    public List<ProgramCourses> getAllCoursesByStudentProgram(Long studentId) {
        Student student = studentsService.getStudentById(studentId);
        return programCoursesRepository.findAllByProgram(student.getProgram());
    }

    @Override
    public ProgramCourses getByProgramIdAndCourseId(Long programId, Long courseId) {
        return programCoursesRepository.findByProgram_IdAndCourse_Id(programId, courseId);
    }
}
