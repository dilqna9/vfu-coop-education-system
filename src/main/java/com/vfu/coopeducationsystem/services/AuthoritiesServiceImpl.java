package com.vfu.coopeducationsystem.services;

import com.vfu.coopeducationsystem.exceptions.EntityNotFoundException;
import com.vfu.coopeducationsystem.models.Authority;
import com.vfu.coopeducationsystem.repositories.AuthoritiesRepository;
import com.vfu.coopeducationsystem.services.contracts.AuthoritiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.NOT_FOUND_ERROR_MSG;

@Service
public class AuthoritiesServiceImpl implements AuthoritiesService {

    private final AuthoritiesRepository authoritiesRepository;

    @Autowired
    public AuthoritiesServiceImpl(AuthoritiesRepository authoritiesRepository) {
        this.authoritiesRepository = authoritiesRepository;
    }

    @Override
    public Authority getAuthorityByUsername(String username) {

        if(!authoritiesRepository.existsByUsername(username)){
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG,  "Authority", "user", username));
        }
        return authoritiesRepository.findByUsername(username);
    }
}
