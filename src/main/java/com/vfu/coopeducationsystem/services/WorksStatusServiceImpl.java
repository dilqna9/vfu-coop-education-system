package com.vfu.coopeducationsystem.services;

import com.vfu.coopeducationsystem.exceptions.EntityNotFoundException;
import com.vfu.coopeducationsystem.models.WorkStatus;
import com.vfu.coopeducationsystem.repositories.WorksStatusRepository;
import com.vfu.coopeducationsystem.services.contracts.WorksStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.NOT_FOUND_ERROR_MSG;

@Service
public class WorksStatusServiceImpl implements WorksStatusService {

    private final WorksStatusRepository worksStatusRepository;

    @Autowired
    public WorksStatusServiceImpl(WorksStatusRepository worksStatusRepository) {
        this.worksStatusRepository = worksStatusRepository;
    }

    @Override
    public List<WorkStatus> getAllWorksStatus() {
        return worksStatusRepository.findAll();
    }

    @Override
    public WorkStatus getWorkStatusByWorkStatusName(String workStatusName) {

        if(!worksStatusRepository.existsByWorkStatusName(workStatusName)){
            throw new EntityNotFoundException(
                    String.format(NOT_FOUND_ERROR_MSG, "Work status", "name", workStatusName));
        }
        return worksStatusRepository.findByWorkStatusName(workStatusName);
    }
}
