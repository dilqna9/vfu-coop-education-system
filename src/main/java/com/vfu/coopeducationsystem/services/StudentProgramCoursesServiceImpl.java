package com.vfu.coopeducationsystem.services;

import com.vfu.coopeducationsystem.exceptions.EntityAlreadyExistsException;
import com.vfu.coopeducationsystem.models.StudentProgramCourse;
import com.vfu.coopeducationsystem.repositories.StudentProgramCoursesRepository;
import com.vfu.coopeducationsystem.services.contracts.StudentProgramCoursesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.vfu.coopeducationsystem.constants.ExceptionsConstants.ALREADY_EXITS_ERROR_MSG;

@Service
public class StudentProgramCoursesServiceImpl implements StudentProgramCoursesService {

    private final StudentProgramCoursesRepository studentProgramCoursesRepository;

    @Autowired
    public StudentProgramCoursesServiceImpl(StudentProgramCoursesRepository studentProgramCoursesRepository) {
        this.studentProgramCoursesRepository = studentProgramCoursesRepository;
    }

    @Override
    public void createStudentProgramCourse(StudentProgramCourse studentProgramCourse) {
        if(studentProgramCoursesRepository
                .existsByStudent_IdAndProgramCourses_Id(studentProgramCourse.getStudent().getId(),
                        studentProgramCourse.getProgramCourses().getId())){
            throw new EntityAlreadyExistsException(
                    String.format(ALREADY_EXITS_ERROR_MSG, "Student program course", "student number", studentProgramCourse.getStudent().getStudentNumber()));
        }
        studentProgramCoursesRepository.saveAndFlush(studentProgramCourse);
    }
}
