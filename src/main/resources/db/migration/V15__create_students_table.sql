CREATE TABLE students
(
    id               bigserial   NOT NULL,
    first_name       varchar(25) NOT NULL,
    middle_name      varchar(25) NOT NULL,
    last_name        varchar(25) NOT NULL,
    student_number   varchar(25) NOT NULL,
    email            varchar(50) NOT NULL,
    phone_number     varchar(25) NOT NULL,
    profile_picture  text        NOT NULL,
    CV               text        NOT NULL,
    download_link    text        NOT NULL,
    semester         int         NOT NULL,
    address_id       int8        NOT NULL,
    program_id       int8        NOT NULL,
    work_status_code varchar(2)  NOT NULL,
    created_by       varchar(50) NOT NULL,
    CONSTRAINT students_pkey PRIMARY KEY (id),
    CONSTRAINT students_address_fkey FOREIGN KEY (address_id) REFERENCES addresses (id),
    CONSTRAINT students_program_fkey FOREIGN KEY (program_id) REFERENCES programs (id),
    CONSTRAINT students_work_status_fkey FOREIGN KEY (work_status_code) REFERENCES work_status (work_status_code),
    CONSTRAINT students_createdby_fkey FOREIGN KEY (created_by) REFERENCES users (username)
);