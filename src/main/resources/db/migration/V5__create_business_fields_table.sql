CREATE TABLE business_fields
(
    id             bigserial NOT NULL,
    business_field text      NOT NULL,
    CONSTRAINT business_fields_pkey PRIMARY KEY (id)
);