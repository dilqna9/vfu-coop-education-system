CREATE TABLE courses
(
    id     bigserial NOT NULL,
    "name" text      NOT NULL,
    CONSTRAINT courses_pkey PRIMARY KEY (id)
);