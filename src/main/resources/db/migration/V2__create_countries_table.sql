CREATE TABLE countries
(
    id           bigserial  NOT NULL,
    name         text       NOT NULL,
    country_code varchar(5) NOT NULL,
    continent_id int8       NOT NULL,
    CONSTRAINT country_pkey PRIMARY KEY (id),
    CONSTRAINT countries_continents_fkey FOREIGN KEY (continent_id) REFERENCES continents (id)
);