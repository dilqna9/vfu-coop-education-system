CREATE TABLE work_status
(
    work_status_code varchar(3) NOT NULL,
    work_status_name text       NOT NULL,
    CONSTRAINT work_status_pkey PRIMARY KEY (work_status_code)
);