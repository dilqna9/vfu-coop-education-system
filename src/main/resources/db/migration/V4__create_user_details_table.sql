CREATE TABLE user_details
(
    id         bigserial NOT NULL,
    first_name varchar   NOT NULL,
    last_name  varchar   NOT NULL,
    email      varchar   NOT NULL,
    CONSTRAINT user_details_pkey PRIMARY KEY (id)
);
