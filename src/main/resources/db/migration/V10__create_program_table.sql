CREATE TABLE programs
(
    id                bigserial  NOT NULL,
    "name"            text       NOT NULL,
    degrees_type_code varchar(3) NOT NULL,
    CONSTRAINT program_pkey PRIMARY KEY (id),
    CONSTRAINT programs_degrees_fkey FOREIGN KEY (degrees_type_code) REFERENCES degrees (degree_type_code)
);