CREATE TABLE student_program_courses
(
    id                bigserial NOT NULL,
    student_id        int8      NOT NULL,
    program_course_id int8      NOT NULL,
    grade             int       NOT NULL,
    CONSTRAINT student_program_courses_pkey PRIMARY KEY (id),
    CONSTRAINT student_program_courses_student_fkey FOREIGN KEY (student_id) REFERENCES program_courses (id),
    CONSTRAINT student_program_courses_program_course_fkey FOREIGN KEY (program_course_id) REFERENCES courses (id)
);
