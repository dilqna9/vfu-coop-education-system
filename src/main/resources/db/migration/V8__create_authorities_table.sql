CREATE TABLE authorities
(
    username  varchar(50) NOT NULL,
    authority varchar(50) NOT NULL,
    CONSTRAINT authorities_pkey PRIMARY KEY (username),
    CONSTRAINT username_authority_key UNIQUE (username, authority),
    CONSTRAINT authority_fk FOREIGN KEY (username) REFERENCES users (username)
);