CREATE TABLE users
(
    username        varchar(50) NOT NULL,
    password        varchar(68) NOT NULL,
    enabled         boolean     NOT NULL,
    user_details_id int8        NOT NULL,
    company_id      int8        NOT NULL,
    CONSTRAINT user_pkey PRIMARY KEY (username),
    CONSTRAINT user_user_details_fkey FOREIGN KEY (user_details_id) REFERENCES user_details (id),
    CONSTRAINT user_company_details_fkey FOREIGN KEY (company_id) REFERENCES company (id)
);