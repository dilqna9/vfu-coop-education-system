CREATE TABLE continents
(
    id             bigserial  NOT NULL,
    name           text       NOT NULL,
    continent_code varchar(2) NOT NULL,
    CONSTRAINT continent_pkey PRIMARY KEY (id)
);