CREATE TABLE degrees
(
    degree_type_code varchar(3) NOT NULL,
    degree_type_name text       NOT NULL,
    CONSTRAINT degrees_type_codes_pkey PRIMARY KEY (degree_type_code)
);