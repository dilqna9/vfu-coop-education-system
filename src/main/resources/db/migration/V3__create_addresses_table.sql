CREATE TABLE addresses
(
    id         bigserial NOT NULL,
    address    text      NOT NULL,
    country_id int8      NOT NULL,
    CONSTRAINT addresses_pkey PRIMARY KEY (id),
    CONSTRAINT addresses_country_fkey FOREIGN KEY (country_id) REFERENCES countries (id)
);