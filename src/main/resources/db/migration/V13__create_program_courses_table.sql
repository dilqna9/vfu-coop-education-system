CREATE TABLE program_courses
(
    id               bigserial  NOT NULL,
    program_id       int8       NOT NULL,
    course_id        int8       NOT NULL,
    course_type_code varchar(2) NOT NULL,
    CONSTRAINT program_courses_pkey PRIMARY KEY (id),
    CONSTRAINT program_courses_program_id_courses_id_key UNIQUE (program_id, course_id),
    CONSTRAINT program_courses_program_fkey FOREIGN KEY (program_id) REFERENCES programs (id),
    CONSTRAINT program_courses_courses_fkey FOREIGN KEY (course_id) REFERENCES courses (id),
    CONSTRAINT program_courses_course_type_fkey FOREIGN KEY (course_type_code) REFERENCES course_type (course_type_code)
);