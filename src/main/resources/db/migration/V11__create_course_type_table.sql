CREATE TABLE course_type
(
    course_type_code varchar(2) NOT NULL,
    course_type_name text       NOT NULL,
    CONSTRAINT courses_type_codes_pkey PRIMARY KEY (course_type_code)
);