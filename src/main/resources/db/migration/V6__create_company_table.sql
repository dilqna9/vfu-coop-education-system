CREATE TABLE company
(
    id                bigserial NOT NULL,
    code              varchar   NOT NULL,
    name              varchar   NOT NULL,
    email             varchar   NOT NULL,
    phone             varchar   NOT NULL,
    fax               varchar,
    logo              text      NOT NULL,
    address_id        int8      NOT NULL,
    business_field_id int8      NOT NULL,
    CONSTRAINT company_pkey PRIMARY KEY (id),
    CONSTRAINT company_address_fkey FOREIGN KEY (address_id) REFERENCES addresses (id),
    CONSTRAINT company_business_field_fkey FOREIGN KEY (business_field_id) REFERENCES business_fields (id)
);